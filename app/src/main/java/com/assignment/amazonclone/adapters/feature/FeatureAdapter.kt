package com.assignment.amazonclone.adapters.feature

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.assignment.amazonclone.R
import com.assignment.amazonclone.data.entities.Category
import kotlinx.android.synthetic.main.item_hor_top.view.*

class FeatureAdapter(
    private var arrayList: ArrayList<Category>
) : RecyclerView.Adapter<FeatureAdapter.CategoryViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): CategoryViewHolder {
        val root = LayoutInflater.from(parent.context).inflate(R.layout.item_hor_cut, parent, false)
        return CategoryViewHolder(root)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bind(arrayList[position])
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }


    inner class CategoryViewHolder(private val binding: View) : RecyclerView.ViewHolder(binding) {
        fun bind(category: Category) {
            binding.category_name.text = category.categoryName
            binding.imageView_top_hor.setImageResource(category.imageUrl)
        }

    }

    fun updateList(categories: ArrayList<Category>) {

        val diffCallback = FeatureDiffCallback(this.arrayList, categories)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        diffResult.dispatchUpdatesTo(this)

        this.arrayList = categories
    }

}