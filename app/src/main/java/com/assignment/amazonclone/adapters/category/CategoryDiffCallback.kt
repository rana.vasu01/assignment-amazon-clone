package com.assignment.amazonclone.adapters.category

import androidx.recyclerview.widget.DiffUtil
import com.assignment.amazonclone.data.entities.Category

open class CategoryDiffCallback(
    private val oldCategory: List<Category>,
    private val newCategory: List<Category>
): DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldCategory.size
    }

    override fun getNewListSize(): Int {
        return newCategory.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldCategory[oldItemPosition] == newCategory[newItemPosition]
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldCategory[oldItemPosition] == newCategory[newItemPosition]
    }
}