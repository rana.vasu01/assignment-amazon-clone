package com.assignment.amazonclone.ui

import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.assignment.amazonclone.adapters.category.CategoryAdapter
import com.assignment.amazonclone.R
import com.assignment.amazonclone.adapters.feature.FeatureAdapter
import com.assignment.amazonclone.data.entities.Category
import com.assignment.amazonclone.data.entities.User
import com.assignment.amazonclone.utils.GlideApp
import com.assignment.amazonclone.utils.Resource
import com.assignment.amazonclone.viewmodels.UserViewModel
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity: AppCompatActivity() {

    private val viewModel: UserViewModel by viewModels()
    private var viewManager = LinearLayoutManager(this)
    private var viewManagerFeature = LinearLayoutManager(this)
    private lateinit var categoryAdapter: CategoryAdapter
    private lateinit var featureAdapter: FeatureAdapter

    private val categories = arrayListOf(
        Category(categoryName = "Essentials", imageUrl = R.drawable.essentials),
        Category(categoryName = "Pantry", imageUrl = R.drawable.pantry),
        Category(categoryName = "HomeWear", imageUrl = R.drawable.homewear),
        Category(categoryName = "miniTV", imageUrl = R.drawable.tv),
        Category(categoryName = "Electronics", imageUrl = R.drawable.electronics),
        Category(categoryName = "FunZone", imageUrl = R.drawable.funzone),
        Category(categoryName = "Video", imageUrl = R.drawable.video),
    )

    private val features = arrayListOf(
        Category(categoryName = "Amazon Pay", imageUrl = R.drawable.amazonpay),
        Category(categoryName = "Send Money", imageUrl = R.drawable.sendmoney),
        Category(categoryName = "Scan UPI QR", imageUrl = R.drawable.scanupiqr),
        Category(categoryName = "Pay Bills", imageUrl = R.drawable.paybills),
        Category(categoryName = "Covid-19", imageUrl = R.drawable.donate),
        Category(categoryName = "Rewards", imageUrl = R.drawable.rewards),
        Category(categoryName = "Get Payment", imageUrl = R.drawable.getpayment),
    )

    val imageList = ArrayList<SlideModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpAdapters()
        setupObservers()
        addDataToTopList()
    }

    private fun setUpAdapters(){
        categoryAdapter = CategoryAdapter(arrayList = ArrayList())
        recyclerViewTop.adapter = categoryAdapter
        viewManager.orientation = LinearLayoutManager.HORIZONTAL
        recyclerViewTop.layoutManager = viewManager
        recyclerViewTop.setHasFixedSize(true)

        featureAdapter = FeatureAdapter(arrayList = ArrayList())
        recyclerViewCut.adapter = featureAdapter
        viewManagerFeature.orientation = LinearLayoutManager.HORIZONTAL
        recyclerViewCut.layoutManager = viewManagerFeature
        recyclerViewCut.setHasFixedSize(true)
    }

    private fun setupObservers() {
        viewModel.getUser("0F8JIqi4zwvb77FGz6Wt").observe(this, {
            it?.let { resource ->
                when (resource.status) {
                    Resource.Status.SUCCESS -> {
                        progressBar.visibility = View.GONE
                        resource.data?.let { user -> setImage(user.body()) }
                    }
                    Resource.Status.ERROR -> {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }
                    Resource.Status.LOADING -> {
                        progressBar.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

    private fun addDataToTopList() {
        for(i in categories){
            val category = Category(categoryName = i.categoryName, imageUrl = i.imageUrl)
            viewModel.addCategory(category)
        }
        if(this::categoryAdapter.isInitialized)
            categoryAdapter.updateList(categories)

        imageList.add(SlideModel("https://bit.ly/2YoJ77H", ScaleTypes.CENTER_CROP))
        imageList.add(SlideModel("https://bit.ly/2BteuF2", ScaleTypes.CENTER_CROP))
        imageList.add(SlideModel("https://bit.ly/3fLJf72", ScaleTypes.CENTER_CROP))
        imageList.add(SlideModel("https://bit.ly/2YoJ77H", ScaleTypes.CENTER_CROP))
        imageList.add(SlideModel("https://bit.ly/2BteuF2", ScaleTypes.CENTER_CROP))
        imageList.add(SlideModel("https://bit.ly/3fLJf72", ScaleTypes.CENTER_CROP))
        image_slider.setImageList(imageList)

        for(i in features){
            val category = Category(categoryName = i.categoryName, imageUrl = i.imageUrl)
            viewModel.addFeature(category)
        }
        if(this::featureAdapter.isInitialized)
            featureAdapter.updateList(features)
    }

    private fun setImage(user: User?) {
        GlideApp.with(this)
            .load(Uri.parse(user?.picture))
            .into(imageView);
    }
}