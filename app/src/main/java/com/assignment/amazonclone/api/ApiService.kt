package com.assignment.amazonclone.api

import com.assignment.amazonclone.BuildConfig
import com.assignment.amazonclone.data.entities.User
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface ApiService {

    @Headers("app-id: ${BuildConfig.DUMMY_API_APP_ID}")
    @GET("data/api/user/{userId}")
    suspend fun getUser(@Path("userId") userId: String): Response<User>
}