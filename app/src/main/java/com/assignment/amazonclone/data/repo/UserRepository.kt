package com.assignment.amazonclone.data.repo

import com.assignment.amazonclone.data.remote.UserRemoteDataSource
import dagger.hilt.android.HiltAndroidApp
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(
    private val remoteDataSource: UserRemoteDataSource
) {
    suspend fun getUser(userId: String) = remoteDataSource.getUser(userId)
}