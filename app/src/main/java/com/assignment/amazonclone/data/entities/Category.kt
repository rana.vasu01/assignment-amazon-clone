package com.assignment.amazonclone.data.entities

data class Category(
    val imageUrl: Int,
    val categoryName: String
)
