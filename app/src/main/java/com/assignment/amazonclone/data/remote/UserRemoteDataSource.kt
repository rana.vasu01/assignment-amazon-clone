package com.assignment.amazonclone.data.remote

import com.assignment.amazonclone.api.ApiService
import javax.inject.Inject

class UserRemoteDataSource @Inject constructor(
    private val apiService: ApiService
) {

    suspend fun getUser(userId: String) = apiService.getUser(userId)

}