package com.assignment.amazonclone.di

import com.assignment.amazonclone.api.ApiService
import com.assignment.amazonclone.data.remote.UserRemoteDataSource
import com.assignment.amazonclone.data.repo.UserRepository
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson) : Retrofit = Retrofit.Builder()
        .baseUrl("https://dummyapi.io/")
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Singleton
    @Provides
    fun provideUserRemoteDataSource(apiService: ApiService) = UserRemoteDataSource(apiService)

    @Singleton
    @Provides
    fun provideRepository(remoteDataSource: UserRemoteDataSource) =
        UserRepository(remoteDataSource)
}