package com.assignment.amazonclone.viewmodels

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.assignment.amazonclone.data.entities.Category
import com.assignment.amazonclone.data.repo.UserRepository
import com.assignment.amazonclone.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(
    private val repository: UserRepository
) : ViewModel(), LifecycleObserver {

    var mutableCategoryList = MutableLiveData<ArrayList<Category>>()
    var newlist = arrayListOf<Category>()

    var mutableFeatureList = MutableLiveData<ArrayList<Category>>()
    var featureList = arrayListOf<Category>()

    fun addCategory(category: Category){
        newlist.add(category)
        mutableCategoryList.value=newlist
    }

    fun addFeature(category: Category){
        featureList.add(category)
        mutableFeatureList.value=featureList
    }

    fun getUser(userId: String) =  liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            val response = repository.getUser(userId)
            emit(Resource.success(data = response))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
}